use anyhow::Result;
use gstreamer::prelude::*;
use gstreamer::MessageView;
use image::{ImageBuffer, Rgb, RgbImage};
use std::env;
use std::fs::read_dir;
use std::fs::DirBuilder;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

pub struct BinEncoder {
    pub bin_content: Vec<String>,
}

#[derive(Copy, Clone, Debug)]
pub enum Colors {
    Black(Rgb<u8>),
    White(Rgb<u8>),
}

struct Color {
    black: Colors,
    white: Colors,
}

#[derive(Debug, Copy, Clone)]
pub struct Pixel {
    pub color: Colors,
}

#[derive(Debug)]
pub struct Frame {
    pub encoded_bits: Vec<Pixel>,
}

impl BinEncoder {
    pub fn read_bin_file(file_path: &String) -> Result<Vec<String>> {
        let mut byte_vec = Vec::new();
        let bin_file = File::open(file_path)?;
        let buf_read = BufReader::new(bin_file);
        let bin_string = buf_read.bytes().collect::<Vec<_>>();
        for byte in bin_string {
            if let Ok(byte) = byte {
                byte_vec.push(format!("{:b}", byte));
            }
        }
        Ok(byte_vec)
    }

    pub fn encode_bits(byte_vec: Vec<String>) -> Result<Vec<Frame>> {
        let mut pixels: Vec<Pixel> = Vec::new();
        let mut frames: Vec<Frame> = Vec::new();
        let frame_pixel_threshold = 1920 * 1080;

        let colors = Color {
            black: Colors::Black(Rgb([0, 0, 0])),
            white: Colors::White(Rgb([255, 255, 255])),
        };

        let mut count = 0;
        for byte in byte_vec {
            let bits = byte.chars();
            for bit in bits {
                count = count + 1;
                if bit == '0' {
                    let pixel = Pixel {
                        color: colors.white,
                    };
                    pixels.push(pixel);
                } else if bit == '1' {
                    let pixel = Pixel {
                        color: colors.black,
                    };
                    pixels.push(pixel);
                }

                if count == frame_pixel_threshold {
                    let frame = Frame {
                        encoded_bits: pixels.clone(),
                    };

                    frames.push(frame);
                    count = 0;
                    pixels.clear();
                }
            }
        }
        Ok(frames)
    }

    pub fn make_images_and_save(frames: &Vec<Frame>) -> Result<()> {
        let mut image_buf: RgbImage = ImageBuffer::new(1920, 1080);
        let mut frame_num = 0;
        let home_dir = env::var("HOME")?;
        let frames_path = format!("{}/.local/share/bintovid/frames/", &home_dir);
        let mut dir_builder = DirBuilder::new();

        if !Path::exists(&Path::new(&frames_path)) {
            dir_builder.recursive(true);
            dir_builder.create(&frames_path)?;
        }

        for frame in frames {
            let mut x = 0;
            let mut y = 1;
            for pixel in frame.encoded_bits.clone() {
                x = x + 1;
                println!("{x} {y} {pixel:?}");
                let color = pixel.color;
                match color {
                    Colors::White(rgb) => {
                        image_buf.put_pixel(x, y, rgb);
                    }
                    Colors::Black(rgb) => {
                        image_buf.put_pixel(x, y, rgb);
                    }
                }
                if x == 1919 {
                    x = 0;
                    y = y + 1;
                }

                if y == 1080 {
                    break;
                }
            }
            _ = image_buf.save(format!(
                "{}/.local/share/bintovid/frames/frame_{frame_num}.png",
                &home_dir
            ));
            frame_num = frame_num + 1;
        }

        Ok(())
    }

    pub fn make_video_from_frames() -> Result<()> {
        let home_dir = env::var("HOME")?;
        let frames_path = format!("{}/.local/share/bintovid/frames/", &home_dir);

        gstreamer::init()?;
        let pipeline = gstreamer::Pipeline::new();

        let multi_file_src_option = gstreamer::ElementFactory::make("multifilesrc");
        let capsfilter = gstreamer::ElementFactory::make("capsfilter");
        let png_dec_option = gstreamer::ElementFactory::make("pngdec");
        let video_convert_option = gstreamer::ElementFactory::make("videoconvert");
        let h264_encoding_option = gstreamer::ElementFactory::make("x264enc");
        let mp4mux_option = gstreamer::ElementFactory::make("mp4mux");
        let filesink_option = gstreamer::ElementFactory::make("filesink");

        let caps = gstreamer::Caps::builder("image/png")
            .field("framerate", gstreamer::Fraction::new(30, 1))
            .field("width", &1920i32)
            .field("height", &1080i32)
            .build();
        let pngsrc = multi_file_src_option
            .property("location", format!("{}{}", frames_path, &"frame_%d.png"))
            .build()?;
        let capsfilter = capsfilter.property("caps", &caps).build()?;
        let filesink = filesink_option
            .property("location", format!("{}{}", frames_path, &"video.mp4"))
            .build()?;
        let pngdec = png_dec_option.build()?;
        let videoconvert = video_convert_option.build()?;
        let x264enc = h264_encoding_option
            .property_from_str("tune", "zerolatency")
            .property("bitrate", &120000u32)
            .build()?;
        let mp4mux = mp4mux_option.build()?;

        let pipeline_options = &[
            &pngsrc,
            &capsfilter,
            &pngdec,
            &videoconvert,
            &x264enc,
            &mp4mux,
            &filesink,
        ];
        pipeline.add_many(pipeline_options)?;

        pngsrc.link(&capsfilter)?;
        capsfilter.link(&pngdec)?;
        pngdec.link(&videoconvert)?;
        videoconvert.link(&x264enc)?;
        x264enc.link(&mp4mux)?;
        mp4mux.link(&filesink)?;

        pipeline.set_state(gstreamer::State::Playing)?;
        if let Some(bus) = pipeline.bus() {
            for msg in bus.iter_timed(gstreamer::ClockTime::NONE) {
                match msg.view() {
                    MessageView::Eos(_) => break,
                    MessageView::Error(err) => {
                        eprintln!("An error occurred while producing the video: {:?}", err);
                        break;
                    }
                    _ => (),
                }
            }
            pipeline.set_state(gstreamer::State::Null)?;
        }

        Ok(())
    }
}
