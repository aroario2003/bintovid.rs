mod encode;

use encode::BinEncoder;
use std::env::args;

fn main() {
    let args = args().collect::<Vec<String>>();
    if let Some(file_path) = args.get(1) {
        let bytes = BinEncoder::read_bin_file(file_path).expect("Could not read binary file");
        let frames = BinEncoder::encode_bits(bytes);
        if let Ok(frames) = frames {
            _ = BinEncoder::make_images_and_save(&frames);
        }
        _ = BinEncoder::make_video_from_frames();
    }
}
